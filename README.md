Hello! My name is Michael Farish, I am 29 years old and I live in Nigeria. Currently working for a company

https://ng.jobsora.com/

My profession is an HR specialist. I have long gone to the acquisition of this profession. On my way, I had to work as a janitor and as a loader - all in order to pay for university studies.
But all the same, after graduation, I got a junior-HR, and worked in this area for more than five years. Today I would like to share with you the experience of HR, namely - what are the pros and cons of this profession?

Advantages of the profession of HR-specialist:
Firstly, it is demand and prestige. Any company needs a specialist in the field of staff recruitment and human resource management.
Secondly, it is important for management. The future of the company depends on the competence of the specialist.
Thirdly, wages. For such work they are simply obliged to pay a lot. However, wages depend on the level and size of the enterprise. As thanks to the HR specialist, the efficiency of the company increases, its growth is accelerated. Therefore, your salary depends on your efforts.
Fourth, rapid career growth.
  
Cons profession HR-specialist:
First, it is an extremely high level of responsibilities. This includes work with documents, and with the notification of people about their dismissal.
Secondly, constant work with people. This requires huge emotional and psychological resources.
Thirdly, it is a high level of stress. Unforeseen plans, business, irregular working hours, meeting with angry employees are all sources of stress for a specialist in this field.
Fourthly, there is a difference of opinion between the HR worker and the management. An HR specialist has his own vision of personnel training, of what the climate in the team should be, and the management may have his own vision. Sometimes the opinion of a specialist may not be taken into account by the management who wants to achieve the goal.

An HR manager is a person who always balances between the interests of employees and business. If HR takes the side of employees, we get the picture when the team is over-fed with “buns”, relaxed, and the business loses its efficiency and money. If HR firmly upholds the position of business, treating employees only as a resource, then the company creates a negative atmosphere, and recruiting is always looking for a replacement for the departed, ”say experienced experts.

Thanks for attention!
